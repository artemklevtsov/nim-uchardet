## Module provides function to detect charset encoding.

import std/streams

when defined(windows):
  const
    dllName = "libuchardet.dll"
elif defined(macosx):
  const
    dllName = "libuchardet.dylib"
else:
  const
    dllName = "libuchardet.so(.0.0.7|)"

type
  CharsetDetector = pointer
  ## Pointer to the Universal Character Detector object.

proc newCharsetDetector(): CharsetDetector {.cdecl, dynlib: dllName, importc: "uchardet_new".}
## The Charset Detector object contructor.

proc delete(ud: CharsetDetector) {.cdecl, dynlib: dllName, importc: "uchardet_delete".}
## The Charset Detector object destructor.

proc reset(ud: CharsetDetector) {.cdecl, dynlib: dllName, importc: "uchardet_reset".}
## Reset the Charset Detector object state.

proc handleDataInternal(ud: CharsetDetector; data: cstring; len: uint): cint {.cdecl, dynlib: dllName, importc: "uchardet_handle_data".}
proc dateEndInternal(ud: CharsetDetector) {.cdecl, dynlib: dllName, importc: "uchardet_data_end".}
proc getCharsetInternal(ud: CharsetDetector): cstring {.cdecl, dynlib: dllName, importc: "uchardet_get_charset".}

const bufsize = 65 * 1024  ## Buffer size
var buffer: array[bufsize, char]

proc getEncoding*(self: CharsetDetector): string =
  ## Get charset detected encoding.
  result = $getCharsetInternal(self)

proc handleData(self: CharsetDetector, a: openArray[char]): bool =
  ## Handle char array data.
  let res = handleDataInternal(self, cast[cstring](a), a.len.uint)
  dateEndInternal(self)
  result = res == 0

proc handleData(self: CharsetDetector, s: string): bool =
  ## Handle string data.
  let res = handleDataInternal(self, s, uint(s.len))
  dateEndInternal(self)
  result = res == 0

proc handleData(self: CharsetDetector, s: Stream, start: int = 0, len: int = -1): bool =
  ## Handle string data.
  result = true
  s.setPosition(start)
  while not s.atEnd():
    let nread = if len > 0: min(len - s.getPosition() + bufsize, bufsize) else: bufsize
    let rbytes = s.readData(addr buffer , nread)
    let res = handleDataInternal(self, addr buffer , rbytes.uint)
    if res != 0:
      result = false
  dateEndInternal(self)

proc handleData(self: CharsetDetector, f: File, start: int = 0, len: int = -1): bool =
  ## Handle file.
  result = true
  f.setFilePos(start)
  while not f.endOfFile():
    let nread = if len > 0: min(len - f.getFilePos() + bufsize, bufsize) else: bufsize
    let rbytes = f.readBuffer(addr buffer, nread)
    let res = handleDataInternal(self, addr buffer, rbytes.uint)
    if res != 0:
      result = false
  dateEndInternal(self)

proc detectEncoding*(s: string, start: int = 0, last: int = -1): string =
  ## Detect string encoding.
  let last = if last == -1: s.high else: last
  var ucd = newCharsetDetector()
  defer: ucd.delete()
  if ucd.handleData(s[start..last]):
    result = ucd.getEncoding()
  else:
    result = "Unknown"

template detect(i: untyped): untyped =
  var ucd = newCharsetDetector()
  defer: ucd.delete()
  if ucd.handleData(i):
    result = ucd.getEncoding()
  else:
    result = "Unknown"

proc detectEncoding*(s: openArray[char]): string =
  ## Detect string encoding.
  detect(s)

proc detectEncoding*(s: Stream): string =
  ## Detect string encoding.
  detect(s)

proc detectEncoding*(f: File): string =
  ## Detect string encoding.
  detect(f)
