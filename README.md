# uchardet

<!-- badges: start -->

[![GitLab CI Build Status](https://gitlab.com/artemklevtsov/nim-uchardet/badges/master/pipeline.svg)](https://gitlab.com/artemklevtsov/nim-uchardet/pipelines)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

<!-- badges: end -->

Nim binding for the [uchardet](https://www.freedesktop.org/wiki/Software/uchardet/) library.

## Installation

```bash
nimble install https://gitlab.com/artemklevtsov/nim-uchardet
```

## Usage

```nim
import uchardet

var ucd = newCharsetDetector()
let s = "\uc548\ub155 \uc0ac\uc6a9\uc790"
ucd.handleData(s)
echo ucd.getEncoding()
ucd.reset()
let ss = newStringStream(s)
ucd.handleData(s)
echo ucd.getEncoding()
ucd.reset()
ucd.delete()
```
