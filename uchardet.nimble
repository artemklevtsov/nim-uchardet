# Package

version       = "0.0.1"
author        = "Artem Klevtsov"
description   = "uchardet is an encoding detector library, which takes a sequence of bytes in an unknown character encoding without any additional information, and attempts to determine the encoding of the text. Returned encoding names are iconv-compatible."
license       = "Apache-2.0"
skipDirs      = @["tests"]

# Dependencies

requires "nim >= 1.4.2"

when defined(nimdistros):
  import distros
  if detectOs(Ubuntu) or detectOs(Debian):
    foreignDep "libuchardet-dev"
  elif detectOs(CentOS) or detectOs(RedHat) or detectOs(Fedora):
    foreignDep "uchardet-devel"
  else:
    foreignDep "uchardet"

task docs, "Generate documentation":
  exec "nim doc --project --index:on --outdir:public uchardet"

task test, "Run all tests":
  exec "nim compile --run --hints:off tests/testall"

task cov, "Generate coverage report":
  exec "coco --verbose --branch --compiler='--hints:off -d:skipTestsOutput' --report_path='public/coverage'"
