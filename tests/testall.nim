import std/unittest
import std/os
import std/strutils
import std/streams
import uchardet

when defined(skipTestsOutput):
  addOutputFormatter(OutputFormatter(newConsoleOutputFormatter(PRINT_FAILURES)))

suite "Detect string encoding":
  test "test ASCII string encoding":
    let s = "ABCD"
    check s.detectEncoding() == "ASCII"
  test "test UTF-8 string encoding":
    let s = "\uc548\ub155 \uc0ac\uc6a9\uc790"
    check s.detectEncoding() == "UTF-8"

suite "Detect string encoding (stream handle)":
  test "test ASCII string stream encoding":
    let s = newStringStream("ABCD")
    check s.detectEncoding() == "ASCII"
  test "test UTF-8 string stream encoding":
    let s = newStringStream("\uc548\ub155 \uc0ac\uc6a9\uc790")
    check s.detectEncoding() == "UTF-8"

suite "Detect char array encoding":
  test "test ASCII char array encoding":
    let a = @['A', 'B', 'C', 'D']
    check a.detectEncoding() == "ASCII"

proc getFileEncoding(path: string): string =
  let (_, fname, _) = splitFile(path)
  fname.toUpper()

# see https://gitlab.freedesktop.org/uchardet/uchardet/-/blob/v0.0.7/test/CMakeLists.txt#L33
const to_skip = [
  "tests/files/ja/utf-16le.txt",
  "tests/files/ja/utf-16be.txt",
  "tests/files/es/iso-8859-15.txt",
  "tests/files/da/iso-8859-1.txt",
  "tests/files/he/iso-8859-8.txt"
]

suite "Detect file encoding (file handle)":
  for path in walkDirRec("tests" / "files"):
    test "test " & path & " file encoding":
      if path in to_skip:
        continue
      let fs = open(path, fmRead)
      defer: fs.close()
      check fs.detectEncoding() == path.getFileEncoding()

suite "Detect file encoding (stream handle)":
  for path in walkDirRec("tests" / "files"):
    test "test " & path & " file stream encoding":
      if path in to_skip:
        continue
      let fs = newFileStream(path, fmRead)
      defer: fs.close()
      check fs.detectEncoding() == path.getFileEncoding()

suite "Detect file encoding (string handle)":
  for path in walkDirRec("tests" / "files"):
    test "test " & path & " file string encoding":
      if path in to_skip:
        continue
      let s = readFile(path)
      check s.detectEncoding() == path.getFileEncoding()
